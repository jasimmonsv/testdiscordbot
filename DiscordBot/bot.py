# bot.py
"""
https://discordpy.readthedocs.io/en/latest/index.html
"""
import os
import logging
import random

import discord

_LOGGER = logging.getLogger()
try:
    if os.environ['LOGGING'].lower() == 'debug':
        _LOGGER.setLevel(logging.DEBUG)
    elif os.environ['LOGGING'].lower() == 'info':
        _LOGGER.setLevel(logging.INFO)
    elif os.environ['LOGGING'].lower() == 'warning':
        _LOGGER.setLevel(logging.WARNING)
    elif os.environ['LOGGING'].lower() == 'error':
        _LOGGER.setLevel(logging.ERROR)
    elif os.environ['LOGGING'].lower() == 'critical':
        _LOGGER.setLevel(logging.CRITICAL)
except KeyError as err:
    _LOGGER.setLevel(logging.INFO)
_LOGGER.info('Logging set at: {} :: environ: {}'.format(_LOGGER.getEffectiveLevel(), os.environ['LOGGING']))

TOKEN = os.getenv('DISCORD_TOKEN')
# GUILD =

client = discord.Client()

def add_role(user, role):
    """
    function that will add a role to a user
    :param user:
    :param role:
    :return:
    """
    pass

def build_role_mechanism(guild, channel):
    """
    Build mechanism to allow users to add roles to their acct

    :param guild: server object to make changes on
    :param channel: channel object to make changes
    :return: N/A
    """
    response = "Select your role: "
    # TODO build
    # await message.channel.send(response)
    # TODO add reactions
    pass

@client.event
async def on_ready():
    _LOGGER.info('{client.user} has connected to Discord!')
    # TODO grab channel from db for guild
    build_role_mechanism("guild", "channel")

@client.event
async def on_guild_join(guild):
    _LOGGER.info("Joined guild {}".format('testing'))
    build_role_mechanism(guild, 'channel')

@client.event
async def on_member_update(before, after):
    _LOGGER.error(before)
    _LOGGER.error(after)

@client.event
async def on_reaction_add(reaction, user):
    print('on_message_edit')
    print(reaction)
    print(user)

@client.event
async def on_raw_reaction_add(payload):
    print('on_raw_reaction_add')
    print(payload)
    # grab message and check if

@client.event
async def on_message_edit(before, after):
    print('on_message_edit')
    print(before)
    print(after)

@client.event
async def on_member_join(member):
    # TODO approve rules
    # TODO deliver roles
    pass


@client.event
async def on_message(message):
    if message.author == client.user:
        return
    brooklyn_99 = [
        'I\'m the human form of the 💯 emoji.',
        'Bingpot!',
        (
            'Cool. Cool cool cool cool cool cool cool, '
            'no doubt no doubt no doubt no doubt.'
        ),
    ]
    if message.content == '99!':
        print(message)
        print(dir(message))
        response = random.choice(brooklyn_99)
        await message.channel.send(response)

@client.event
async def on_user_update(before, after):
    _LOGGER.error(before)
    _LOGGER.error(after)


if __name__ == '__main__':
    client.run(TOKEN)
